/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "GaudiKernel/MsgStream.h"
#include "eformat/Status.h"

#include <format>


template <class IDMAP> 
FullEventAssembler<IDMAP>::FullEventAssembler():
  m_runnum(0),
  m_lvl1_id(0),
  m_rod_version(0),
  m_rob_version(0),
  m_detEvtType(0),
  m_lvl1_trigger_type(0),
  m_bcid(0)
{
}


template <class IDMAP> 
FullEventAssembler<IDMAP>::~FullEventAssembler()
{
  clear();
}


template <class IDMAP>
void FullEventAssembler<IDMAP>::clear()
{
   // clear stack
   for(const auto& [id, rod] : m_rodMap) delete rod;
   m_rodMap.clear();

   for(const auto& [id, rob] : m_robMap) delete rob;
   m_robMap.clear();
}


template <class IDMAP> 
void FullEventAssembler<IDMAP>::fill(RawEventWrite* re,  MsgStream& log )
{
   m_runnum  = re->run_no() ; 
   m_lvl1_id = re->lvl1_id(); 
   m_lvl1_trigger_type = re->lvl1_trigger_type();
   m_bcid = re->bc_id();

   // ROD to ROB
   RodToRob(m_rodMap,m_robMap,log); 
   // ROB to FullEventFragment 
   RobToEvt(m_robMap,re,log); 
} 


template <class IDMAP> 
typename FullEventAssembler<IDMAP>::RODDATA* 
FullEventAssembler<IDMAP>::getRodData(uint32_t id) 
{   
  RODDATA* theROD = nullptr;

  RODMAP::const_iterator itr = m_rodMap.find(id);
  if(itr!=m_rodMap.end())
    {
      theROD = itr->second;
    } 
  else 
    {
      theROD = new RODDATA(); 	   
      m_rodMap.emplace(id, theROD);
    }
  
  return theROD;
}


template <class IDMAP> 
void 
FullEventAssembler<IDMAP>::RodToRob(RODMAP& rodMap,
                                    ROBMAP& robMap,  MsgStream& log ) const
{ 
  // loop over all the RODs
  for(const auto& [rodid, rod] : rodMap)
    {
      if(log.level() <= MSG::DEBUG)
          log << MSG::DEBUG << " Rod id = " << std::format("{:x}", rodid) << endmsg;

      // make an ROB id from an ROD id. 
      const uint32_t robid = m_idmap.getRobID(rodid);
      
      // find or make a ROS fragment
      ROBMAP::const_iterator itr = robMap.find(robid);
      if(itr!=robMap.end())
        {
          log << MSG::ERROR << " More than one ROD in ROB:" << std::format("{:x}", itr->first) << endmsg;
        }
      else 
        { // Make a new ROBFragment 
          auto theROB = new OFFLINE_FRAGMENTS_NAMESPACE_WRITE::ROBFragment(robid,m_runnum,m_lvl1_id,m_bcid,
                                                                           m_lvl1_trigger_type,m_detEvtType,rod->size(),
                                                                           &(*rod)[0],eformat::STATUS_BACK);
          theROB->minor_version(m_rob_version);
          theROB->rod_minor_version(m_rod_version);
          robMap.emplace(robid, theROB);
        } 
    }
} 


template <class IDMAP> 
void FullEventAssembler<IDMAP>::RobToEvt(ROBMAP& robMap, RawEventWrite* re, MsgStream& log ) const
{
  const bool debug = log.level() <= MSG::DEBUG;
  if(debug) log << MSG::DEBUG << " RobToEvt, number of ROBFrag= " << robMap.size() << endmsg;

  for(const auto& [robid, theROB] : robMap)
    {
      if(debug) log << MSG::DEBUG << " RobToEvt: adding ROB "
                    << std::format("{:x}", theROB->source_id()) << endmsg;
      re->append(theROB);
    } 

  if(debug) log << MSG::DEBUG << " FullEventFragment is done " << re << endmsg;
}


template <class IDMAP> 
FullEventAssembler<IDMAP>::RODMAP::const_iterator 
FullEventAssembler<IDMAP>::begin() const 
{
  return  m_rodMap.begin();
}


template <class IDMAP> 
FullEventAssembler<IDMAP>::RODMAP::const_iterator 
FullEventAssembler<IDMAP>::end() const 
{ 
  return  m_rodMap.end();
}


template <class IDMAP> 
void FullEventAssembler<IDMAP>::setRodMinorVersion(uint16_t m )
{  
  m_rod_version = m;
}


template <class IDMAP> 
void FullEventAssembler<IDMAP>::setRobMinorVersion(uint16_t m )
{  
  m_rob_version = m;
}


template <class IDMAP> 
void FullEventAssembler<IDMAP>::setDetEvtType(uint32_t m )
{
  m_detEvtType = m;
}


template <class IDMAP> 
void FullEventAssembler<IDMAP>::setLvl1TriggerType(uint8_t m )
{
  m_lvl1_trigger_type = m;
}


template <class IDMAP> 
typename FullEventAssembler<IDMAP>::IDMAP_t& FullEventAssembler<IDMAP>::idMap()
{ 
  return m_idmap ;
}

